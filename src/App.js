import Home from './pages/Home';
// import Orders from './pages/Orders';
import Foods from './pages/Foods';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Login from './pages/Login';
import Error from './pages/Error';
import {Container} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar'
import Dash from './pages/Dash';
import AddFood from './pages/AddFoods';
import EditFood from './pages/EditFood';
import FoodView from './components/FoodView';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';


function App(){

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

 const unsetUser = () =>  {
  localStorage.clear();
 }


 useEffect(()=>{
  fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
    headers:{
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res=>res.json())
  .then(data=>{
    console.log(data);

    if(typeof data._id !== "undefined"){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    }else{
      setUser({
        id: null,
        isAdmin: null
      })
    }
  })
 },
 []
 );


  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
       <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/selection" element={<Foods/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="*" element={<Error/>}/>
            <Route path="/Admin" element={<Dash/>}/>
            <Route path="/AddFood" element={<AddFood/>}/>
            <Route path="/EditFood/:foodId" element={<EditFood/>}/>
            <Route path="/foods/:foodId" element={<FoodView/>}/>
          </Routes>
        </Container>
    </Router>

    </UserProvider>
 
  );
}

export default App;
