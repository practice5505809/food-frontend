import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function AddFood() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');

    const [isActive, setIsActive] = useState(false);
    const [imageUrl, setImageUrl] = useState('');

	function addFood(e) {

	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/foods`, {
	    	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    imageUrl: imageUrl
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Food item succesfully added",
	    		    icon: "success",
	    		    text: `${name} is now added`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    setName('');
	    setDescription('');

	}

	// Submit button validation
	useEffect(() => {

        if(name !== "" && description !== "" && imageUrl !== "" > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description]);

    return (
    	user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Add a Food Item</h1>
		        <Form onSubmit={(e) => addFood(e)}>
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Food Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Food Name" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label>Food Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Enter Food Description" 
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group className="mb-3" controlId="Image URL">
              			<Form.Label>Image URL</Form.Label>
             			<Form.Control
                		type="string"
                		onChange={(e) => setImageUrl(e.target.value)}
              			/>
            		</Form.Group>



		            {/* conditionally render submit button based on isActive state */}
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Save
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Save
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/foods" />
	    	
    )

}
