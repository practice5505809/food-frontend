import Banner from '../components/Banner.js'

export default function Error() {

    const data = {
        title: "Oops! Page not Found!",
        content: "The page you are looking cannot be found.",
        destination: "/",
        label: "Return to Home Page"
    }
    return (
    <Banner bannerProp = {data}/>
    )
}

