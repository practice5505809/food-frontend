import Banner from '../components/Banner';
import Advertisement from '../components/Advertisement'

export default function Home(){

	const data = {
        title: "This page is only for you.",
        content: "Credits to all the posts from What's your ulam, pare!",
        destination: "/selection",
        label: "Check all food selections here"
    }

	return(
		<>
			<Banner bannerProp={data}/>
			<Advertisement/>

		</>
	)
}
