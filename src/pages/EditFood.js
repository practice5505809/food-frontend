import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Form, Button } from 'react-bootstrap';

export default function EditFood() {

	const {user} = useContext(UserContext);

	const { foodId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	// const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

	function EditFood(e) {

	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/foods/${foodId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Food item succesfully u",
	    		    icon: "success",
	    		    text: `${name} is now updated`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })
	    setName('');
	    setDescription('');

	}

	// Submit button validation
	useEffect(() => {

        if(name !== "" && description !== "" > 0 ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description]);

    useEffect(()=> {

    	console.log(foodId);

    	fetch(`${process.env.REACT_APP_API_URL}/foods/${foodId}`)
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

  
    		setName(data.name);
    		setDescription(data.description);

    	});

    }, [foodId]);

    return (
    	user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Edit Food Item</h1>
		        <Form onSubmit={(e) => EditFood(e)}>
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Food Name" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label>Product Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Enter Food Description" 
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

*/}
		            {/* conditionally render submit button based on isActive state */}
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/Admin"/>
	    	
    )

}
