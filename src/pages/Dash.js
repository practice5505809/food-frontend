import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Dash(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	const [allFoods, setAllFoods] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const fetchData = () =>{
		// Get all products in the database
		fetch(`${process.env.REACT_APP_API_URL}/foods/allfoodselection`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllFoods(data.map(food => {
				return(
					<tr key={food._id}>
						<td>{food._id}</td>
						<td>{food.name}</td>
						<td>{food.description}</td>
						<td>{food.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// We use conditional rendering to set which button should be visible based on the product status (active/inactive)
								(food.isActive)
								?	
								 	// A button to change the product status to "Inactive"
									<Button variant="danger" size="sm" onClick ={() => archive(food._id, food.name)}>Archive</Button>
								:
									<>
										{/* A button to change the food status to "Active"*/}
										<Button variant="success" size="sm" onClick ={() => unarchive(food._id, food.name)}>Unarchive</Button>
										{/* A button to edit a specific food*/}
										<Button as={ Link } to={`/editFood/${food._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the product inactive
	const archive = (foodId, foodName) =>{
		console.log(foodId);
		console.log(foodName);

		fetch(`${process.env.REACT_APP_API_URL}/foods/${foodId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${foodName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const unarchive = (foodId, foodName) =>{
		console.log(foodId);
		console.log(foodName);

		fetch(`${process.env.REACT_APP_API_URL}/foods/${foodId}/activate`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${foodName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all foods in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all foods.
		fetchData();
	},[])
	//***[] dependencies are optional

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*A button to add a new food*/}
				<Button as={Link} to="/AddFood" variant="dark" size="lg" className="mx-2">Add Food</Button>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Food ID</th>
		         <th>Food Name</th>
		         <th>Description</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allFoods }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/foods" />
	)
}
