import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import FoodCard from '../components/FoodCard';
import UserContext from '../UserContext';

export default function Foods(){

	const [foods, setFoods] =useState([])

	const {user} = useContext(UserContext);

	useEffect(()=>{
	fetch(`${process.env.REACT_APP_API_URL}/foods/selection`)
	.then(res=>{

		return(res.json())
		

	})
	.then(data=>{
		
	const foodArr = (data.map(food => {
		return (
			<FoodCard foodProp={food} key={food._id}/>
			)
		}))
		setFoods(foodArr)
	})
	},[foods.length])

	//***add a way to /admin
	return(
		(user.isAdmin)?
		<Navigate to="/Admin"/>
		:
		<>
			<h1>Food Selection</h1>
			{foods}
		</>
	)
}
