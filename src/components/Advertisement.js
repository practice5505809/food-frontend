import {Row, Col, Card} from 'react-bootstrap';
import React from 'react';
import greeting from './greeting.png';
import japgreeting from './japgreeting.png';
import korgreeting from './korgreeting.png';



export default function Advertisement(){
  return(
  <Row className = "mt-3 mb-3">
    <Col xs={6} md={4}>
      <Card className="Advertisementcard">
        <Card.Body>
        <img src={greeting} alt="..." class="w-100"/>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={6} md={4}>
      <Card className="Advertisementcard">
        <Card.Body>
        <img src={japgreeting} alt="..." class="w-100"/>
        </Card.Body>
      </Card>
    </Col>
     <Col xs={6} md={4}>
      <Card className="Advertisementcard">
        <Card.Body>
        <img src={korgreeting} alt="..." class="w-100"/>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    );
}

