import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function FoodView(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const { foodId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");

	const [userId, setUserId] = useState("");

	const order = (foodId) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/order`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: foodId,
				userId: localStorage.getItem("userId"),
			
			})
		})
		.then(res=>res.json())
		.then(data=>{
			
			if(data===true){

				Swal.fire({
				  title: "Order Successful!",
				  icon: "success",
				  text: "Thank you for your purchase!"
				});
				navigate("/foods")

			}else{

				Swal.fire({
				  title: "Something went wrong!",
				  icon: "error",
				  text: "User login is required!"
				});

			}

		})
	}

	useEffect(()=>{
		console.log(foodId);
		fetch(`${process.env.REACT_APP_API_URL}/foods/${foodId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
	
				
		})


	},[foodId])


	return(
		<Container className="mt-5">
		  <Row>
		     <Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
					
						
					</Card.Body>

				</Card>
		     </Col>
		  </Row>
		  		

		</Container>

	)

}

