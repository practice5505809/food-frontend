import { Link } from 'react-router-dom';
import { Row, Col, Card, Button } from 'react-bootstrap';


export default function FoodCard({foodProp}){

	let { name, imageUrl, description, _id} = foodProp;

	return(
		<Row className = "mt-3 mb-3">
    	<Col xs={12} md={4}>
		<Card className="p-3 mb-3">
			<Card.Body className="card-foods">
      			<Card.Img className="product-card-image" variant="left" src={imageUrl} class = "w-100"/>
				<Card.Title>{name}</Card.Title>
				<Button variant="dark" as={Link} to={`/foods/${_id}`}>Open for description</Button>
			</Card.Body>
		</Card>
		</Col>
     </Row>	
     
	)
}





