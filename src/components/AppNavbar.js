import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useContext } from "react";
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import logo from './cupcake.gif'; 


export default function AppNavbar() {

const { user } = useContext(UserContext);

return(

<Navbar bg="dark" variant="dark" expand="lg">
        <Container>
        <img src={logo} alt="Logo" width="75"/>
          <Navbar.Brand as={Link} to="/">Jerhu</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              
              </Nav>

                            <Nav>
              {/*we use the id of the user to conditionaly render the login, register and logout buttons*/}
              {(user.id !== null) ?
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register">You're mine until 2069</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>


)
}